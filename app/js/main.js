var breadcrumbs = [
'Главная >',
'Ноутбуки, компьютеры >',
'Ноутбуки'
];

var countModel = '4 685';
var nameCity = 'Усть-Каменогорск';
var source   = document.getElementById("template_1").innerHTML;
var template = Handlebars.compile(source);
var context  = {city: nameCity, breadcrumbs: breadcrumbs, countModel:countModel};
var html     = template(context);
$("#root").html(html);

$(document).ready(function() {
	$('.description__full-text').click(function(event) {
		event.preventDefault();
		if($('.description__hidden-text').hasClass('hide'))
		{	
			$('.description__hidden-text').removeClass('hide');
			$('.description__full-text').text('Скрыть');
		}
		else{
			$('.description__hidden-text').addClass('hide');
			$('.description__full-text').text('Читать полностью');
		}
	})
})

